package com.johnniem.bookmakeroddsguru;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

public class DatesActivity extends AppCompatActivity {

    public static final String DatumStrExtra = "datumString";

    DatePicker datePicker;
    Button buttonGo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dates);

        buttonGo = (Button) findViewById(R.id.buttonGo);

        datePicker = (DatePicker) findViewById(R.id.datePickerView);

        buttonGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get selected date
                String dateStr = Integer.toString(datePicker.getYear())
                        + "-" + Integer.toString(datePicker.getMonth() + 1)
                        + "-" + Integer.toString(datePicker.getDayOfMonth());

                Intent intent = new Intent(DatesActivity.this, DogadajiOnDateActivity.class);
                intent.putExtra(DatumStrExtra, dateStr);
                startActivity(intent);
            }
        });
    }
}
