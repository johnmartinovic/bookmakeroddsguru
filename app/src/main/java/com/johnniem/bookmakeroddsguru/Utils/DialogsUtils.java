package com.johnniem.bookmakeroddsguru.Utils;

import android.app.ProgressDialog;
import android.content.Context;

public class DialogsUtils {

    public static ProgressDialog getProgressDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);

        progressDialog.setTitle("Downloading data");
        progressDialog.setMessage("Please, wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        return progressDialog;
    }
}
