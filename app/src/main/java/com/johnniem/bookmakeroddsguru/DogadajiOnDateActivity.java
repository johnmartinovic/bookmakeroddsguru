package com.johnniem.bookmakeroddsguru;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.johnniem.bookmakeroddsguru.POJOs.Dogadaj;
import com.johnniem.bookmakeroddsguru.POJOs.DogadajAdapter;
import com.johnniem.bookmakeroddsguru.RetrofitStuff.RetrofitAPI;
import com.johnniem.bookmakeroddsguru.Utils.DialogsUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DogadajiOnDateActivity extends AppCompatActivity {

    private static final String URL = "http://johnniem.com:8080/SuperSportWebService/";

    ArrayList<Dogadaj> dogadajList;
    ListView listView;
    private DogadajAdapter adapter;

    ProgressDialog progressDialog;

    String dateStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dogadaji_on_date);

        progressDialog = DialogsUtils.getProgressDialog(this);

        progressDialog.show();

        dateStr = getIntent().getStringExtra(DatesActivity.DatumStrExtra);

        listView = (ListView) findViewById(R.id.list_view);
        listView.setEmptyView(findViewById(R.id.emptyElement));

        getDogadajListByDateStr();
    }

    void getDogadajListByDateStr() {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RetrofitAPI service = retrofit.create(RetrofitAPI.class);

        Call<ArrayList<Dogadaj>> call = service.dogadajList(dateStr);

        call.enqueue(new Callback<ArrayList<Dogadaj>>() {
            @Override
            public void onResponse(Call<ArrayList<Dogadaj>> call, Response<ArrayList<Dogadaj>> response) {
                dogadajList = response.body();

                if (!dogadajList.isEmpty()) {
                    adapter = new DogadajAdapter(getBaseContext(), dogadajList);

                    listView.setAdapter(adapter);

                    adapter.notifyDataSetChanged();
                }

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<Dogadaj>> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
