package com.johnniem.bookmakeroddsguru;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.johnniem.bookmakeroddsguru.POJOs.Dogadaj;
import com.johnniem.bookmakeroddsguru.POJOs.DogadajAdapter;
import com.johnniem.bookmakeroddsguru.POJOs.Tipovi;
import com.johnniem.bookmakeroddsguru.POJOs.TipoviAdapter;
import com.johnniem.bookmakeroddsguru.RetrofitStuff.RetrofitAPI;
import com.johnniem.bookmakeroddsguru.Utils.DialogsUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DogadajTipoviActivity extends AppCompatActivity {

    private static final String URL = "http://johnniem.com:8080/SuperSportWebService/";

    ArrayList<Tipovi> tipoviList;
    ListView tipoviListView;
    private static TipoviAdapter tipoviAdapter;

    ProgressDialog progressDialog;

    int dogadajId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dogadaj_tipovi);
        progressDialog = DialogsUtils.getProgressDialog(this);

        progressDialog.show();

        dogadajId = getIntent().getIntExtra(DogadajAdapter.DogadajIdExtra, 0);

        tipoviListView = (ListView) findViewById(R.id.tipovi_list_view);
        tipoviListView.setEmptyView(findViewById(R.id.emptyElement2));

        getTipoviListByDogadajId();
    }

    void getTipoviListByDogadajId() {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RetrofitAPI service = retrofit.create(RetrofitAPI.class);

        Call<ArrayList<Tipovi>> call = service.tipoviList(dogadajId);

        call.enqueue(new Callback<ArrayList<Tipovi>>() {
            @Override
            public void onResponse(Call<ArrayList<Tipovi>> call, Response<ArrayList<Tipovi>> response) {
                tipoviList = response.body();

                if (!tipoviList.isEmpty()) {
                    tipoviAdapter = new TipoviAdapter(getBaseContext(), tipoviList);

                    tipoviListView.setAdapter(tipoviAdapter);

                    tipoviAdapter.notifyDataSetChanged();
                }

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<Tipovi>> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
