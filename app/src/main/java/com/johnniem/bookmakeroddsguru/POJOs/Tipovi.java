package com.johnniem.bookmakeroddsguru.POJOs;

import java.sql.Timestamp;

public class Tipovi {

    private String vrijednostiTipova;
    private long vrijemeDownloada;

    public Tipovi() {
    }

    public Tipovi(String vrijednostiTipova, long vrijemeDownloada) {
        this.vrijednostiTipova = vrijednostiTipova;
        this.vrijemeDownloada = vrijemeDownloada;
    }

    public String getVrijednostiTipova() {
        return vrijednostiTipova;
    }

    public void setVrijednostiTipova(String vrijednostiTipova) {
        this.vrijednostiTipova = vrijednostiTipova;
    }

    public long getVrijemeDownloada() {
        return vrijemeDownloada;
    }

    public void setVrijemeDownloada(long vrijemeDownloada) {
        this.vrijemeDownloada = vrijemeDownloada;
    }
}