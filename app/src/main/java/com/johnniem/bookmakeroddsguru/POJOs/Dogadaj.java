package com.johnniem.bookmakeroddsguru.POJOs;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Dogadaj implements Parcelable {

    private String sport;
    private String liga;
    private String nazivVrsteTipova;
    private String vrsteTipova;
    private int id;
    private Date datumPrikaza;
    private String broj;
    private String naziv;
    private String dobitniTipovi;
    private String rezultat;
    private String dodatanOpisDogadaja;
    private String detaljnoVrijemePocetka;
    private String webVrijemePocetka;
    private String dodatanOpisLige;

    public Dogadaj() {
    }

    public Dogadaj(String sport, String liga, String nazivVrsteTipova, String vrsteTipova, int id,
                   Date datumPrikaza, String broj, String naziv, String dobitniTipovi, String rezultat,
                   String dodatanOpisDogadaja, String detaljnoVrijemePocetka, String webVrijemePocetka,
                   String dodatanOpisLige) {
        this.sport = sport;
        this.liga = liga;
        this.nazivVrsteTipova = nazivVrsteTipova;
        this.vrsteTipova = vrsteTipova;
        this.id = id;
        this.datumPrikaza = datumPrikaza;
        this.broj = broj;
        this.naziv = naziv;
        this.dobitniTipovi = dobitniTipovi;
        this.rezultat = rezultat;
        this.dodatanOpisDogadaja = dodatanOpisDogadaja;
        this.detaljnoVrijemePocetka = detaljnoVrijemePocetka;
        this.webVrijemePocetka = webVrijemePocetka;
        this.dodatanOpisLige = dodatanOpisLige;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getLiga() {
        return liga;
    }

    public void setLiga(String liga) {
        this.liga = liga;
    }

    public String getNazivVrsteTipova() {
        return nazivVrsteTipova;
    }

    public void setNazivVrsteTipova(String nazivVrsteTipova) {
        this.nazivVrsteTipova = nazivVrsteTipova;
    }

    public String getVrsteTipova() {
        return vrsteTipova;
    }

    public void setVrsteTipova(String vrsteTipova) {
        this.vrsteTipova = vrsteTipova;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatumPrikaza() {
        return datumPrikaza;
    }

    public void setDatumPrikaza(Date datumPrikaza) {
        this.datumPrikaza = datumPrikaza;
    }

    public String getBroj() {
        return broj;
    }

    public void setBroj(String broj) {
        this.broj = broj;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getDobitniTipovi() {
        return dobitniTipovi;
    }

    public void setDobitniTipovi(String dobitniTipovi) {
        this.dobitniTipovi = dobitniTipovi;
    }

    public String getRezultat() {
        return rezultat;
    }

    public void setRezultat(String rezultat) {
        this.rezultat = rezultat;
    }

    public String getDodatanOpisDogadaja() {
        return dodatanOpisDogadaja;
    }

    public void setDodatanOpisDogadaja(String dodatanOpisDogadaja) {
        this.dodatanOpisDogadaja = dodatanOpisDogadaja;
    }

    public String getDetaljnoVrijemePocetka() {
        return detaljnoVrijemePocetka;
    }

    public void setDetaljnoVrijemePocetka(String detaljnoVrijemePocetka) {
        this.detaljnoVrijemePocetka = detaljnoVrijemePocetka;
    }

    public String getWebVrijemePocetka() {
        return webVrijemePocetka;
    }

    public void setWebVrijemePocetka(String webVrijemePocetka) {
        this.webVrijemePocetka = webVrijemePocetka;
    }

    public String getDodatanOpisLige() {
        return dodatanOpisLige;
    }

    public void setDodatanOpisLige(String dodatanOpisLige) {
        this.dodatanOpisLige = dodatanOpisLige;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(sport);
        out.writeString(liga);
        out.writeString(nazivVrsteTipova);
        out.writeString(vrsteTipova);
        out.writeInt(id);
        out.writeSerializable(datumPrikaza);
        out.writeString(broj);
        out.writeString(naziv);
        out.writeString(dobitniTipovi);
        out.writeString(rezultat);
        out.writeString(dodatanOpisDogadaja);
        out.writeString(detaljnoVrijemePocetka);
        out.writeString(webVrijemePocetka);
        out.writeString(dodatanOpisLige);
    }

    private Dogadaj(Parcel in) {
        sport = in.readString();;
        liga = in.readString();
        nazivVrsteTipova = in.readString();
        vrsteTipova = in.readString();
        id = in.readInt();
        datumPrikaza = (java.util.Date) in.readSerializable();
        broj = in.readString();
        naziv = in.readString();
        dobitniTipovi = in.readString();
        rezultat = in.readString();
        dodatanOpisDogadaja = in.readString();
        detaljnoVrijemePocetka = in.readString();
        webVrijemePocetka = in.readString();
        dodatanOpisLige = in.readString();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Dogadaj createFromParcel(Parcel in) {
            return new Dogadaj(in);
        }

        @Override
        public Dogadaj[] newArray(int size) {
            return new Dogadaj[size];
        }
    };
}