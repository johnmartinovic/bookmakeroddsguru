package com.johnniem.bookmakeroddsguru.POJOs;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.johnniem.bookmakeroddsguru.DatesActivity;
import com.johnniem.bookmakeroddsguru.DogadajTipoviActivity;
import com.johnniem.bookmakeroddsguru.DogadajiOnDateActivity;
import com.johnniem.bookmakeroddsguru.R;

import java.util.ArrayList;

public class DogadajAdapter extends ArrayAdapter<Dogadaj> implements View.OnClickListener {

    public static final String DogadajIdExtra = "dogadajId";

    private ArrayList<Dogadaj> dogadajList;
    Context mContext;

    public DogadajAdapter(Context context, ArrayList<Dogadaj> dogadajList) {
        super(context, R.layout.dogadaj_list_layout, dogadajList);

        this.dogadajList = dogadajList;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder;

        if ((convertView == null) || (convertView.getTag() == null)) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.dogadaj_list_layout, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.brojTextView     = (TextView) convertView.findViewById(R.id.broj_text);
            viewHolder.nazivTextView    = (TextView) convertView.findViewById(R.id.naziv_text);
            viewHolder.sportTextView    = (TextView) convertView.findViewById(R.id.sport_text);
            viewHolder.ligaTextView     = (TextView) convertView.findViewById(R.id.liga_text);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Get the data item for this position
        Dogadaj dogadaj = getItem(position);

        viewHolder.brojTextView.setText(dogadaj.getBroj());
        viewHolder.nazivTextView.setText(dogadaj.getNaziv());
        viewHolder.sportTextView.setText(dogadaj.getSport());
        viewHolder.ligaTextView.setText(dogadaj.getLiga());

        viewHolder.brojTextView.setTag(position);
        convertView.setOnClickListener(this);

        convertView.setTag(viewHolder);

        // Return the completed view to render on screen
        return convertView;
    }

    // View lookup cache
    private static class ViewHolder {
        TextView brojTextView;
        TextView nazivTextView;
        TextView sportTextView;
        TextView ligaTextView;
    }

    @Override
    public void onClick(View view) {
        ViewHolder viewHolder =  (ViewHolder) view.getTag();
        Integer position = (Integer) viewHolder.brojTextView.getTag();
        Dogadaj dogadaj = getItem(position);

        //Toast.makeText(getContext(), dogadaj.getNaziv(), Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(view.getContext(), DogadajTipoviActivity.class);
        intent.putExtra(DogadajIdExtra, dogadaj.getId());
        view.getContext().startActivity(intent);
    }
}
