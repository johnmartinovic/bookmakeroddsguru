package com.johnniem.bookmakeroddsguru.POJOs;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.johnniem.bookmakeroddsguru.DatesActivity;
import com.johnniem.bookmakeroddsguru.DogadajTipoviActivity;
import com.johnniem.bookmakeroddsguru.DogadajiOnDateActivity;
import com.johnniem.bookmakeroddsguru.R;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TipoviAdapter extends ArrayAdapter<Tipovi> implements View.OnClickListener {

    private ArrayList<Tipovi> tipoviList;
    Context mContext;

    public TipoviAdapter(Context context, ArrayList<Tipovi> tipoviList) {
        super(context, R.layout.dogadaj_list_layout, tipoviList);

        this.tipoviList = tipoviList;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder;

        if ((convertView == null) || (convertView.getTag() == null)) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.tipovi_list_layout, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.vrijemeTextView  = (TextView) convertView.findViewById(R.id.vrijeme_text);
            viewHolder.koefTextView     = (TextView) convertView.findViewById(R.id.koef_text);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Get the data item for this position
        Tipovi tipovi = getItem(position);

        viewHolder.vrijemeTextView.setText(tipovi.getVrijednostiTipova());
        viewHolder.koefTextView.setText(convertTime(tipovi.getVrijemeDownloada()));

        viewHolder.vrijemeTextView.setTag(position);
        convertView.setOnClickListener(this);

        convertView.setTag(viewHolder);

        // Return the completed view to render on screen
        return convertView;
    }

    // View lookup cache
    private static class ViewHolder {
        TextView vrijemeTextView;
        TextView koefTextView;
    }

    @Override
    public void onClick(View view) {
        ViewHolder viewHolder =  (ViewHolder) view.getTag();
        Integer position = (Integer) viewHolder.vrijemeTextView.getTag();
        Tipovi tipovi = getItem(position);

        Toast.makeText(getContext(), tipovi.getVrijednostiTipova(), Toast.LENGTH_SHORT).show();
    }

    public String convertTime(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }
}
