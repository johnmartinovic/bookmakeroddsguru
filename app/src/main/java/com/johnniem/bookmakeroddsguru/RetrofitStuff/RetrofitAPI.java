package com.johnniem.bookmakeroddsguru.RetrofitStuff;

import com.johnniem.bookmakeroddsguru.POJOs.Dogadaj;
import com.johnniem.bookmakeroddsguru.POJOs.Tipovi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitAPI {

    @GET("getDogadajTipoviListById")
    Call<ArrayList<Tipovi>> tipoviList(
            @Query("id") int id
    );

    @GET("getDogadajListByDate")
    Call<ArrayList<Dogadaj>> dogadajList(
            @Query("datumPrikaza") String datumPrikaza
    );
}